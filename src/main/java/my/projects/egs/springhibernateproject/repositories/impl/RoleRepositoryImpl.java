package my.projects.egs.springhibernateproject.repositories.impl;

import my.projects.egs.springhibernateproject.entities.Role;
import my.projects.egs.springhibernateproject.repositories.RoleRepository;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class RoleRepositoryImpl implements RoleRepository {
    private static Logger logger = Logger.getLogger(RoleRepositoryImpl.class.getName());

    private SessionFactory sessionFactory;

    @Autowired
    public RoleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Role save(Role role) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            int id = (int) session.save(role);
            role.setId(id);
            transaction.commit();
            logger.info("Transaction for role save method committed");
        } catch (Exception commitException) {
            logger.error(commitException.getMessage());
            role = null;
            if (transaction != null) {
                try {
                    transaction.rollback();
                } catch (Exception rollbackException) {
                    logger.error(rollbackException.getMessage());
                }
            }
        }
        return role;
    }

    @Override
    public Role findByRole(String roleType) {
        Role role = null;
        try (Session session = sessionFactory.openSession()) {
            role = session.createNamedQuery("my.projects.egs.springhibernateproject.entities.Role.findByRole", Role.class)
                    .setParameter("role", roleType)
                    .getSingleResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return role;
    }

    @Override
    public List<Role> findAll() {
        List<Role> roles = new ArrayList<>();
        try (Session session = sessionFactory.openSession()) {
            roles = session.createNamedQuery("my.projects.egs.springhibernateproject.entities.Role.findAllRoles", Role.class)
                    .getResultList();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return roles;
    }

    @Override
    public boolean delete(Role role) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.delete(role);
            transaction.commit();
            logger.info("Transaction for role delete method committed");
        } catch (Exception e) {
            logger.error(e.getMessage());
            if (transaction != null) {
                try {
                    transaction.rollback();
                } catch (Exception rollbackException) {
                    logger.error(rollbackException.getMessage());
                }
            }
            return false;
        }
        return true;
    }
}
