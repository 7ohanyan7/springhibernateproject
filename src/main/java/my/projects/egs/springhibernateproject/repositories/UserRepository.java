package my.projects.egs.springhibernateproject.repositories;

import my.projects.egs.springhibernateproject.entities.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    
    Optional<User> save(User user);

    Optional<User>  findById(long id);

    Optional<User> findByLogin(String login);

    List<User> findAll();
    
    boolean delete(long id);

    Optional<User> update(User user);
    
}
