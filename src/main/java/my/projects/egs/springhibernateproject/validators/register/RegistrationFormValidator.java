package my.projects.egs.springhibernateproject.validators.register;

import my.projects.egs.springhibernateproject.dtos.request.UserRequestDto;
import my.projects.egs.springhibernateproject.errors.ErrorCode;
import my.projects.egs.springhibernateproject.errors.ErrorProvider;
import my.projects.egs.springhibernateproject.errors.FormError;
import my.projects.egs.springhibernateproject.util.RegistrationConstants;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class RegistrationFormValidator {

    private static Logger logger = Logger.getLogger(RegistrationFormValidator.class.getName());
    private final ErrorProvider<FormError> formErrors;

    public RegistrationFormValidator() {
        formErrors = new ErrorProvider<>();
    }

    public boolean isValid(UserRequestDto userRequestDto) {
        formErrors.getErrors().clear();
        validateLogin(userRequestDto.getLogin());
        validatePassword(userRequestDto.getPassword());
        validatePasswordsMatch(userRequestDto.getPassword(), userRequestDto.getConfirmPassword());
        validateEmail(userRequestDto.getEmail());
        validateFirstName(userRequestDto.getFirstName());
        validateLastName(userRequestDto.getLastName());
        validateBirthDate(userRequestDto.getBirthDate());
        validateCountry(userRequestDto.getAddress().getCountry());
        validateCity(userRequestDto.getAddress().getCity());
        validateRegion(userRequestDto.getAddress().getRegion());
        validateStreet(userRequestDto.getAddress().getStreet());

        return formErrors.getErrors().isEmpty();
    }

    private void validateLogin(String login) {
        if (login.length() < RegistrationConstants.LOGIN_MIN_SIZE || login.length() > RegistrationConstants.LOGIN_MAX_SIZE) {
            logger.warn("Login field is not valid");
            formErrors.addError(ErrorCode.BAD_LOGIN.getErrorCode(), new FormError(ErrorCode.BAD_LOGIN.getErrorCode(), ErrorCode.BAD_LOGIN.getErrorMessage()));
        }
    }

    private void validatePassword(String password) {
        if (password.length() < RegistrationConstants.PASSWORD_MIN_SIZE) {
            logger.warn("Password field is not valid");
            formErrors.addError(ErrorCode.BAD_PASSWORD.getErrorCode(), new FormError(ErrorCode.BAD_PASSWORD.getErrorCode(), ErrorCode.BAD_PASSWORD.getErrorMessage()));
        }
    }

    private void validatePasswordsMatch(String password1, String password2) {
        if (!password1.equals(password2)) {
            logger.warn("Confirm password field is not valid");
            formErrors.addError(ErrorCode.BAD_CONFIRM_PASSWORD.getErrorCode(), new FormError(ErrorCode.BAD_CONFIRM_PASSWORD.getErrorCode(), ErrorCode.BAD_CONFIRM_PASSWORD.getErrorMessage()));
        }
    }

    private void validateFirstName(String firstName) {
        if (firstName.isEmpty()) {
            logger.warn("First name field is not valid");
            formErrors.addError(ErrorCode.BAD_FIRST_NAME.getErrorCode(), new FormError(ErrorCode.BAD_FIRST_NAME.getErrorCode(), ErrorCode.BAD_FIRST_NAME.getErrorMessage()));
        }
    }

    private void validateLastName(String lastName) {
        if (lastName.isEmpty()) {
            logger.warn("Last name field is not valid");
            formErrors.addError(ErrorCode.BAD_LAST_NAME.getErrorCode(), new FormError(ErrorCode.BAD_LAST_NAME.getErrorCode(), ErrorCode.BAD_LAST_NAME.getErrorMessage()));
        }
    }

    private void validateBirthDate(String birthDate) {
        boolean isValid = true;
        List<Integer> MONTH_DAYS_31 = new ArrayList<Integer>();
        MONTH_DAYS_31.add(1);
        MONTH_DAYS_31.add(3);
        MONTH_DAYS_31.add(5);
        MONTH_DAYS_31.add(7);
        MONTH_DAYS_31.add(8);
        MONTH_DAYS_31.add(10);
        MONTH_DAYS_31.add(12);

        String[] dateSplited = birthDate.split("-");
        int birthDay = 0;
        int birthMonth = 0;
        int birthYear = 0;
        if (dateSplited.length == 3) {
            birthYear = Integer.parseInt(dateSplited[0]);
            birthMonth = Integer.parseInt(dateSplited[1]);
            birthDay = Integer.parseInt(dateSplited[2]);
        }

        if (birthDay >= 1 && birthDay <= 31) {
            if ( (birthDay > 28 && birthMonth == 2) || (birthDay == 31 && !MONTH_DAYS_31.contains(birthMonth)) ) {
                logger.warn("Birth day field is not valid");
                isValid = false;
            }
        } else {
            logger.warn("Birth day field is not valid");
            isValid = false;
        }

        if (birthMonth < 1 || birthMonth > 12) {
            logger.warn("Birth month field is not valid");
            isValid = false;
        }

        if (birthYear < RegistrationConstants.YEAR_MIN_SIZE || birthYear > RegistrationConstants.YEAR_MAX_SIZE) {
            logger.warn("Birth year field is not valid");
            isValid = false;
        }

        if (!isValid) {
            formErrors.addError(ErrorCode.BAD_BIRTH_DATE.getErrorCode(), new FormError(ErrorCode.BAD_BIRTH_DATE.getErrorCode(), ErrorCode.BAD_BIRTH_DATE.getErrorMessage()));
        }
    }

    private void validateEmail(String email) {
        if (email.isEmpty()) {
            logger.warn("Email name field is not valid");
            formErrors.addError(ErrorCode.BAD_EMAIL.getErrorCode(), new FormError(ErrorCode.BAD_EMAIL.getErrorCode(), ErrorCode.BAD_EMAIL.getErrorMessage()));
        }
    }

    private void validateCountry(String country) {
        if (country.isEmpty()) {
            logger.warn("Country name field is not valid");
            formErrors.addError(ErrorCode.BAD_COUNTRY.getErrorCode(), new FormError(ErrorCode.BAD_COUNTRY.getErrorCode(), ErrorCode.BAD_COUNTRY.getErrorMessage()));
        }
    }

    private void validateCity(String city) {
        if (city.isEmpty()) {
            logger.warn("City name field is not valid");
            formErrors.addError(ErrorCode.BAD_CITY.getErrorCode(), new FormError(ErrorCode.BAD_CITY.getErrorCode(), ErrorCode.BAD_CITY.getErrorMessage()));
        }
    }

    private void validateRegion(String region) {
        if (region.isEmpty()) {
            logger.warn("Region name field is not valid");
            formErrors.addError(ErrorCode.BAD_REGION.getErrorCode(), new FormError(ErrorCode.BAD_REGION.getErrorCode(), ErrorCode.BAD_REGION.getErrorMessage()));
        }
    }

    private void validateStreet(String street) {
        if (street.isEmpty()) {
            logger.warn("Street name field is not valid");
            formErrors.addError(ErrorCode.BAD_STREET.getErrorCode(), new FormError(ErrorCode.BAD_STREET.getErrorCode(), ErrorCode.BAD_STREET.getErrorMessage()));
        }
    }

    public Map<Integer, FormError> getErrors() {
        return formErrors.getErrors();
    }


}
