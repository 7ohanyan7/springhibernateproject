package my.projects.egs.springhibernateproject.repositories;

import my.projects.egs.springhibernateproject.entities.Role;

import java.util.List;

public interface RoleRepository {

    Role save(Role role);

    Role findByRole(String roleType);

    List<Role> findAll();

    boolean delete(Role role);

}
