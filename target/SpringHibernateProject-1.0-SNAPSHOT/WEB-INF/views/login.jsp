<!DOCTYPE html>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%><%@ page isELIgnored="false" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
</head>
<body>
<div>
    <form:form action="/login" method="post" modelAttribute="user">
        <table>
            <tr>
                <td>
                    Login:
                </td>
                <td>
                    <form:input path="login" type="text"/>
                </td>
            </tr>
            <tr>
                <td>
                    Password:
                </td>
                <td>
                    <form:input path="password" type="Password"/>
                </td>
            </tr>
        </table>
        <p style="color: red">${errorMessage}</p>
        <input type="submit" value="Login">
        <a href="/register">Yet not registered?</a>
    </form:form>
</div>
</body>
</html>