package my.projects.egs.springhibernateproject.mappers;

import my.projects.egs.springhibernateproject.dtos.response.UserResponseDto;
import my.projects.egs.springhibernateproject.entities.User;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Component
public class UserResponseMapper {

    public UserResponseDto userToUserResponseDto(User user){
        UserResponseDto userResponseDto = null;
        if (user != null)
        {
            userResponseDto = new UserResponseDto();
            userResponseDto.setId(user.getId());
            userResponseDto.setLogin(user.getLogin());
            userResponseDto.setEmail(user.getEmail());
            userResponseDto.setFirstName(user.getFirstName());
            userResponseDto.setLastName(user.getLastName());
            userResponseDto.setAddress(user.getAddress());
            userResponseDto.setBirthDate(user.getBirthDate().toString());
        }

        return userResponseDto;
    }

    public List<UserResponseDto> usersToUserResponseDtos(List<User> users){
        List<UserResponseDto> userResponseDtos = new LinkedList<UserResponseDto>();

        for (User user : users) {
            userResponseDtos.add(userToUserResponseDto(user));
        }

        return userResponseDtos;
    }

    public User userResponseDtoToUser(UserResponseDto userResponseDto){
        User user = null;
        if (userResponseDto != null)
        {
            user = new User();
            user.setId(userResponseDto.getId());
            user.setLogin(userResponseDto.getLogin());
            user.setEmail(userResponseDto.getEmail());
            user.setFirstName(userResponseDto.getFirstName());
            user.setLastName(userResponseDto.getLastName());
            user.setAddress(userResponseDto.getAddress());
            user.setBirthDate(LocalDate.parse(userResponseDto.getBirthDate()));
        }

        return user;
    }

    public List<User> userResponseDtosToUsers(List<UserResponseDto> userResponseDtos){
        List<User> users = new LinkedList<User>();

        for (UserResponseDto userResponseDto : userResponseDtos) {
            users.add(userResponseDtoToUser(userResponseDto));
        }

        return users;
    }
}
