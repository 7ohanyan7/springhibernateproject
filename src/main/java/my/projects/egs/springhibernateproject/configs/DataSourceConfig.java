package my.projects.egs.springhibernateproject.configs;

import my.projects.egs.springhibernateproject.entities.Address;
import my.projects.egs.springhibernateproject.entities.Role;
import my.projects.egs.springhibernateproject.entities.User;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

@Configuration
@ComponentScan(basePackages = {"my.projects.egs.springhibernateproject.repositories"})
public class DataSourceConfig {
    private SessionFactory sessionFactory;

    public DataSourceConfig() throws IOException {
        Properties properties = new Properties();
        properties.load(new FileReader(Objects.requireNonNull(getClass().getClassLoader().getResource("hibernate.properties"), "hibernate.properties file not found").getFile()));
        org.hibernate.cfg.Configuration configuration = new org.hibernate.cfg.Configuration()
                .addProperties(properties)
                .addAnnotatedClass(User.class)
                .addAnnotatedClass(Role.class)
                .addAnnotatedClass(Address.class);

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();

        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
    }

    @Bean
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
