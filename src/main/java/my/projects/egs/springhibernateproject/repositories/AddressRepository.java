package my.projects.egs.springhibernateproject.repositories;

import my.projects.egs.springhibernateproject.entities.Address;

import java.util.Optional;

public interface AddressRepository {

    Optional<Address> findByAddress(Address address);

}
