package my.projects.egs.springhibernateproject.exceptions;

public class UserExistException extends RegistrationException{
    public UserExistException(String message) {
        super(message);
    }
}
