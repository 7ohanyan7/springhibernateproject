package my.projects.egs.springhibernateproject.errors;

import java.util.HashMap;
import java.util.Map;

public class ErrorProvider<ErrorType extends Error> {

    private Map<Integer, ErrorType> errors;


    public ErrorProvider() {
        errors = new HashMap<>();
    }

    public void addError(Integer errorCode, ErrorType error) {
        errors.put(errorCode, error);
    }

    public Map<Integer, ErrorType> getErrors() {
        return errors;
    }
}
