package my.projects.egs.springhibernateproject.mappers;

import my.projects.egs.springhibernateproject.dtos.request.UserRequestDto;
import my.projects.egs.springhibernateproject.entities.User;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Component
public class UserRequestMapper {
    public UserRequestDto userToUserRequestDto(User user){
        UserRequestDto userRequestDto = null;
        if (user != null)
        {
            userRequestDto = new UserRequestDto();
            userRequestDto.setId(user.getId());
            userRequestDto.setLogin(user.getLogin());
            userRequestDto.setPassword(user.getPassword());
            userRequestDto.setEmail(user.getEmail());
            userRequestDto.setFirstName(user.getFirstName());
            userRequestDto.setLastName(user.getLastName());
            userRequestDto.setAddress(user.getAddress());
            userRequestDto.setBirthDate(user.getBirthDate().toString());
            userRequestDto.setRoles(user.getRoles());
        }

        return userRequestDto;
    }

    public List<UserRequestDto> usersToUserRequestDtos(List<User> users){
        List<UserRequestDto> userRequestDtos = new LinkedList<UserRequestDto>();

        for (User user :
                users) {
            userRequestDtos.add(userToUserRequestDto(user));
        }

        return userRequestDtos;
    }

    public User userRequestDtoToUser(UserRequestDto userRequestDto){
        User user = null;
        if (userRequestDto != null)
        {
            user = new User();
            user.setId(userRequestDto.getId());
            user.setLogin(userRequestDto.getLogin());
            user.setPassword(userRequestDto.getPassword());
            user.setEmail(userRequestDto.getEmail());
            user.setFirstName(userRequestDto.getFirstName());
            user.setLastName(userRequestDto.getLastName());
            user.setAddress(userRequestDto.getAddress());
            user.setBirthDate(LocalDate.parse(userRequestDto.getBirthDate()));
            user.setRoles(userRequestDto.getRoles());
        }

        return user;
    }

    public List<User> userRequestDtosToUsers(List<UserRequestDto> userRequestDtos){
        List<User> users = new LinkedList<User>();

        for (UserRequestDto userRequestDto :
                userRequestDtos) {
            users.add(userRequestDtoToUser(userRequestDto));
        }

        return users;
    }
}
