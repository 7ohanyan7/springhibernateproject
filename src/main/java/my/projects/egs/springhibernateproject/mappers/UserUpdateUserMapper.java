package my.projects.egs.springhibernateproject.mappers;

import my.projects.egs.springhibernateproject.dtos.request.UserUpdateDto;
import my.projects.egs.springhibernateproject.entities.User;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class UserUpdateUserMapper {

    public User userUpdateDtoToUser(UserUpdateDto userUpdateDto){
        User user = null;
        if (userUpdateDto != null)
        {
            user = new User();
            user.setId(userUpdateDto.getId());
            user.setLogin(userUpdateDto.getLogin());
            user.setEmail(userUpdateDto.getEmail());
            user.setFirstName(userUpdateDto.getFirstName());
            user.setLastName(userUpdateDto.getLastName());
        }

        return user;
    }

    public User userUpdateDtoToUser(UserUpdateDto userUpdateDto, User user){
        if (userUpdateDto != null && user != null)
        {
            user.setId(userUpdateDto.getId());
            user.setLogin(userUpdateDto.getLogin());
            user.setEmail(userUpdateDto.getEmail());
            user.setFirstName(userUpdateDto.getFirstName());
            user.setLastName(userUpdateDto.getLastName());
        }

        return user;
    }

    public List<User> userUpdateDtosToUsers(List<UserUpdateDto> userUpdateDtos){
        List<User> users = new LinkedList<User>();

        for (UserUpdateDto userUpdateDto : userUpdateDtos) {
            users.add(userUpdateDtoToUser(userUpdateDto));
        }

        return users;
    }

    public UserUpdateDto userToUserUpdateDto(User user){
        UserUpdateDto userUpdateDto = null;
        if (user != null)
        {
            userUpdateDto = new UserUpdateDto();
            userUpdateDto.setId(user.getId());
            userUpdateDto.setLogin(user.getLogin());
            userUpdateDto.setEmail(user.getEmail());
            userUpdateDto.setFirstName(user.getFirstName());
            userUpdateDto.setLastName(user.getLastName());
        }

        return userUpdateDto;
    }

    public List<UserUpdateDto> usersToUserUpdateDtos(List<User> users){
        List<UserUpdateDto> userUpdateDtos = new LinkedList<UserUpdateDto>();

        for (User user : users) {
            userUpdateDtos.add(userToUserUpdateDto(user));
        }

        return userUpdateDtos;
    }
}