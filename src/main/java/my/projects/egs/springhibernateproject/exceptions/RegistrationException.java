package my.projects.egs.springhibernateproject.exceptions;

public class RegistrationException extends Exception{
    public RegistrationException(String message) {
        super(message);
    }
}
