package my.projects.egs.springhibernateproject.entities;

import javax.persistence.*;

@org.hibernate.annotations.NamedQueries({
        @org.hibernate.annotations.NamedQuery(name = "my.projects.egs.springhibernateproject.entities.Address.findByAddress",
                query = "from Address addr where country = :country and addr.city = :city and addr.region = :region and addr.street = :street")})
@Entity
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "country", nullable = false, length = 50)
//    @Max(value = 50, message = "{country.max}")
    private String country;
    @Column(name = "city", nullable = false, length = 50)
//    @Max(value = 50, message = "{city.max}")
    private String city;
    @Column(name = "region", nullable = false, length = 50)
//    @Max(value = 50, message = "{region.max}")
    private String region;
    @Column(name = "street", nullable = false, length = 50)
//    @Max(value = 50, message = "{street.max}")
    private String street;

    public Address() {}

    public Address(String country, String city, String region, String street) {
        this.country = country;
        this.city = city;
        this.region = region;
        this.street = street;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

}
