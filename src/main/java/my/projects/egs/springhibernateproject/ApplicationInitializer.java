package my.projects.egs.springhibernateproject;

import my.projects.egs.springhibernateproject.configs.DataSourceConfig;
import my.projects.egs.springhibernateproject.configs.WebMvcConfig;
import my.projects.egs.springhibernateproject.configs.WebSecurityConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

@ComponentScan(basePackages = {"my.projects.egs.springhibernateproject.configs"})
public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    @Override
    protected Class<?>[] getRootConfigClasses() {
        System.out.println("getRootConfigClasses");
        return new Class[] {WebSecurityConfig.class, DataSourceConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        System.out.println("getServletConfigClasses");
        return new Class[] {WebMvcConfig.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[] {"/"};
    }
}
