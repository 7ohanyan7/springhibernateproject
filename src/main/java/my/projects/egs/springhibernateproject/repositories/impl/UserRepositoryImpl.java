package my.projects.egs.springhibernateproject.repositories.impl;

import my.projects.egs.springhibernateproject.entities.Role;
import my.projects.egs.springhibernateproject.entities.User;
import my.projects.egs.springhibernateproject.repositories.AddressRepository;
import my.projects.egs.springhibernateproject.repositories.RoleRepository;
import my.projects.egs.springhibernateproject.repositories.UserRepository;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private static Logger logger = Logger.getLogger(UserRepositoryImpl.class.getName());

    private SessionFactory sessionFactory;
    private RoleRepository roleRepository;
    private AddressRepository addressRepository;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory, RoleRepository roleRepository, AddressRepository addressRepository) {
        this.sessionFactory = sessionFactory;
        this.roleRepository = roleRepository;
        this.addressRepository = addressRepository;
    }

    @Override
    public Optional<User> save(User user) {
        Transaction transaction = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            Role roleFromDB = null;
            List<Role> rolesForDB = new ArrayList<>();
            for (Role role : user.getRoles()) {
                roleFromDB = roleRepository.findByRole(role.getRole());
                if (roleFromDB != null) {
                    rolesForDB.add(roleFromDB);
                } else {
                    rolesForDB.add(role);
                }
            }
            user.setRoles(rolesForDB);

            addressRepository.findByAddress(user.getAddress()).ifPresent(user::setAddress);
            long id = (Long) session.save(user);
            user.setId(id);
            transaction.commit();
            logger.info("Transaction for user save method committed");
        } catch (Exception ex1) {
            logger.error(ex1.getMessage());
            if (transaction != null) {
                try {
                    transaction.rollback();
                } catch (Exception ex2) {
                    logger.error(ex2.getMessage());
                }
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return Optional.ofNullable(user);
    }

    @Override
    public Optional<User> findById(long id) {
        User user = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            user = session.createNamedQuery("my.projects.egs.springhibernateproject.entities.User.findById", User.class)
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return Optional.ofNullable(user);
    }

    @Override
    public Optional<User> findByLogin(String login) {
        User user = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            user = session.createNamedQuery("my.projects.egs.springhibernateproject.entities.User.findByLogin", User.class)
                    .setParameter("login", login)
                    .getSingleResult();
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return Optional.ofNullable(user);
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        Session session = null;
        try {
            session = sessionFactory.openSession();
            users = session.createNamedQuery("my.projects.egs.springhibernateproject.entities.User.findAllUsers", User.class)
                    .getResultList();
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return users;
    }

    @Override
    public boolean delete(long id) {
        Transaction transaction = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            Optional<User> userForDeleteOptional = findById(id);
            if (userForDeleteOptional.isPresent()) {
                session.delete(userForDeleteOptional.get());
            }
            transaction.commit();
            logger.info("Transaction for user delete method committed");
        } catch (Exception e) {
            logger.error(e.getMessage());
            if (transaction != null) {
                try {
                    transaction.rollback();
                } catch (Exception rollbackException) {
                    logger.error(rollbackException.getMessage());
                }
            }
            return false;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return true;
    }

    @Override
    public Optional<User> update(User user) {
        Transaction transaction = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            Optional<User> userInDBOptional = findById(user.getId());
            if (userInDBOptional.isPresent()) {
                session.update(user);
            }
            transaction.commit();
            logger.info("Transaction for user update method committed");
        } catch (Exception commitException) {
            logger.error(commitException.getMessage());
            user = null;
            if (transaction != null) {
                try {
                    transaction.rollback();
                } catch (Exception rollbackException) {
                    logger.error(rollbackException.getMessage());
                }
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return Optional.ofNullable(user);
    }

}
