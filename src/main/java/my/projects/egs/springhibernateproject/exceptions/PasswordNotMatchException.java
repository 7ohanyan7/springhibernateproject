package my.projects.egs.springhibernateproject.exceptions;

public class PasswordNotMatchException extends LoginException{
    public PasswordNotMatchException(String message) {
        super(message);
    }
}
