<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: hranto
  Date: 10/9/2019
  Time: 11:38 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin</title>
</head>
<body>
<table style="width:100%">
    <tr>
        <th>Login</th>
        <th>Email</th>
        <th>First name</th>
        <th>Last name</th>
        <th>Actions</th>
    </tr>
    <c:forEach var="user" items="${users}">
        <tr>
            <form:form action="/users" method="post" modelAttribute="userUpdate">
                <td hidden ><form:input path="id" value="${user.id}"/></td>
                <td><form:input type="text" value="${user.login}" path="login"/></td>
                <td><form:input type="text" value="${user.email}" path="email"/></td>
                <td><form:input type="text" value="${user.firstName}" path="firstName"/></td>
                <td><form:input type="text" value="${user.lastName}" path="lastName"/></td>
                <td><input type="submit" value="Update"/></td>
            </form:form>
            <form:form action="/users/delete/${user.id}" method="post">
                <td>
                    <input type="submit" value="Delete"/>
                </td>
            </form:form>
        </tr>
    </c:forEach>
</table>
</body>
</html>
