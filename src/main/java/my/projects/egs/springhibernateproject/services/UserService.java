package my.projects.egs.springhibernateproject.services;

import my.projects.egs.springhibernateproject.dtos.request.LoginPasswordDto;
import my.projects.egs.springhibernateproject.dtos.request.UserRequestDto;
import my.projects.egs.springhibernateproject.dtos.request.UserUpdateDto;
import my.projects.egs.springhibernateproject.dtos.response.UserResponseDto;
import my.projects.egs.springhibernateproject.exceptions.LoginException;
import my.projects.egs.springhibernateproject.exceptions.NotAuthenticatedException;
import my.projects.egs.springhibernateproject.exceptions.RegistrationException;

import java.util.List;
import java.util.Optional;

public interface UserService {

    Optional<UserResponseDto> save(UserRequestDto userRequestDto);

    Optional<UserResponseDto>  findById(long id);

    Optional<UserResponseDto> findByLogin(String login);

    List<UserResponseDto> findAll();

    boolean delete(long id);

    Optional<UserResponseDto> update(UserUpdateDto userUpdateDto);

    void register(UserRequestDto userRequestDto) throws RegistrationException;

    UserResponseDto login(LoginPasswordDto loginPasswordDto) throws LoginException;

    UserResponseDto getUser() throws NotAuthenticatedException;
}
