<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>User</title>
</head>
<body>
    <p>${errorMessage}</p>
    <p>Welcome, ${userInfo.firstName} ${userInfo.lastName}</p>
    <p>Your login is ${userInfo.login}</p>
    <p>Your email is ${userInfo.email}</p>
    <p>Your birth date is on ${userInfo.birthDate}</p>
    <p>Your address is ${userInfo.address.country}, ${userInfo.address.city}, ${userInfo.address.region}, ${userInfo.address.street}</p>
    <a href="/users">Users</a>
</body>
</html>