package my.projects.egs.springhibernateproject.forms.register;

import my.projects.egs.springhibernateproject.errors.ErrorCode;

public enum RegistrationForm {
    LOGIN("Login : ", "login", "text", "User", ErrorCode.BAD_LOGIN.getErrorCode()),
    PASSWORD("Password : ", "password", "password", "User", ErrorCode.BAD_PASSWORD.getErrorCode()),
    CONFIRM_PASSWORD("Confirm password : ", "confirmPassword", "password", "User", ErrorCode.BAD_CONFIRM_PASSWORD.getErrorCode()),
    EMAIL("Email : ", "email", "email", "User", ErrorCode.BAD_EMAIL.getErrorCode()),
    FIRST_NAME("First name : ", "firstName", "text", "User", ErrorCode.BAD_FIRST_NAME.getErrorCode()),
    LAST_NAME("Last name : ", "lastName", "text", "User", ErrorCode.BAD_LAST_NAME.getErrorCode()),
    BIRTH_DATE("Birth date : ", "birthDate", "date", "User", ErrorCode.BAD_BIRTH_DATE.getErrorCode()),
    COUNTRY("Country : ", "country", "text", "Address", ErrorCode.BAD_COUNTRY.getErrorCode()),
    CITY("City : ", "city", "text", "Address", ErrorCode.BAD_CITY.getErrorCode()),
    REGION("Region : ", "region", "text", "Address", ErrorCode.BAD_REGION.getErrorCode()),
    STREET("Street : ", "street", "text", "Address", ErrorCode.BAD_STREET.getErrorCode());


    private String fieldName;
    private String fieldType;
    private String fieldNameForView;
    private String fieldClassType;
    private int fieldErrorCode;

    RegistrationForm(String fieldNameForView, String fieldName, String fieldType, String fieldClassType, int fieldErrorCode) {
        this.fieldNameForView = fieldNameForView;
        this.fieldName = fieldName;
        this.fieldType = fieldType;
        this.fieldClassType = fieldClassType;
        this.fieldErrorCode = fieldErrorCode;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }

    public String getFieldNameForView() {
        return fieldNameForView;
    }

    public String getFieldClassType() {
        return fieldClassType;
    }

    public int getFieldErrorCode() {
        return fieldErrorCode;
    }
}
