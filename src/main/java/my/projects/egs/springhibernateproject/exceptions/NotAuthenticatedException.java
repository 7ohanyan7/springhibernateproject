package my.projects.egs.springhibernateproject.exceptions;

public class NotAuthenticatedException extends Exception{
    public NotAuthenticatedException(String message) {
        super(message);
    }
}
