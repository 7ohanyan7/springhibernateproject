package my.projects.egs.springhibernateproject.util;

public interface RegistrationConstants {
    int LOGIN_MIN_SIZE = 3;
    int LOGIN_MAX_SIZE = 50;
    int PASSWORD_MIN_SIZE = 8;
    int YEAR_MIN_SIZE = 1900;
    int YEAR_MAX_SIZE = 2000;
}
