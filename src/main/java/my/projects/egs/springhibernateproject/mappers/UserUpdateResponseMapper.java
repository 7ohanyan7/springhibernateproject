package my.projects.egs.springhibernateproject.mappers;

import my.projects.egs.springhibernateproject.dtos.request.UserUpdateDto;
import my.projects.egs.springhibernateproject.dtos.response.UserResponseDto;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class UserUpdateResponseMapper {

    public UserResponseDto userUpdateDtoToUserResponseDto(UserUpdateDto userUpdateDto){
        UserResponseDto userResponseDto = null;
        if (userUpdateDto != null)
        {
            userResponseDto = new UserResponseDto();
            userResponseDto.setId(userUpdateDto.getId());
            userResponseDto.setLogin(userUpdateDto.getLogin());
            userResponseDto.setEmail(userUpdateDto.getEmail());
            userResponseDto.setFirstName(userUpdateDto.getFirstName());
            userResponseDto.setLastName(userUpdateDto.getLastName());
        }

        return userResponseDto;
    }

    public List<UserResponseDto> userUpdateDtosToUserResponseDtos(List<UserUpdateDto> userUpdateDtos){
        List<UserResponseDto> userResponseDtos = new LinkedList<UserResponseDto>();

        for (UserUpdateDto userUpdateDto : userUpdateDtos) {
            userResponseDtos.add(userUpdateDtoToUserResponseDto(userUpdateDto));
        }

        return userResponseDtos;
    }

    public UserUpdateDto userResponseDtoToUserUpdateDto(UserResponseDto userResponseDto){
        UserUpdateDto userUpdateDto = null;
        if (userResponseDto != null)
        {
            userUpdateDto = new UserUpdateDto();
            userUpdateDto.setId(userResponseDto.getId());
            userUpdateDto.setLogin(userResponseDto.getLogin());
            userUpdateDto.setEmail(userResponseDto.getEmail());
            userUpdateDto.setFirstName(userResponseDto.getFirstName());
            userUpdateDto.setLastName(userResponseDto.getLastName());
        }

        return userUpdateDto;
    }

    public List<UserUpdateDto> userResponseDtosToUserUpdateDtos(List<UserResponseDto> userResponseDtos){
        List<UserUpdateDto> userUpdateDtos = new LinkedList<UserUpdateDto>();

        for (UserResponseDto userResponseDto : userResponseDtos) {
            userUpdateDtos.add(userResponseDtoToUserUpdateDto(userResponseDto));
        }

        return userUpdateDtos;
    }
}