package my.projects.egs.springhibernateproject.controllers;

import my.projects.egs.springhibernateproject.dtos.request.UserUpdateDto;
import my.projects.egs.springhibernateproject.dtos.response.UserResponseDto;
import my.projects.egs.springhibernateproject.exceptions.NotAuthenticatedException;
import my.projects.egs.springhibernateproject.mappers.UserUpdateUserMapper;
import my.projects.egs.springhibernateproject.mappers.UserUpdateResponseMapper;
import my.projects.egs.springhibernateproject.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class UserController {

    private UserService userService;
    private UserUpdateResponseMapper userUpdateResponseMapper;

    @Autowired
    public UserController(UserService userService, UserUpdateResponseMapper userUpdateResponseMapper) {
        this.userService = userService;
        this.userUpdateResponseMapper = userUpdateResponseMapper;
    }

    @GetMapping(value = "/welcome")
    public String welcome(Model model){
        try {
            UserResponseDto userResponseDto = userService.getUser();
            model.addAttribute("userInfo", userResponseDto);
            return "welcome";
        } catch (NotAuthenticatedException e) {
            e.printStackTrace();
            model.addAttribute("errorMessage", e.getMessage());
            return "login";
        }
    }

    @GetMapping(value = "/users")
    public String admin(Model model) {
        model.addAttribute("users", userUpdateResponseMapper.userResponseDtosToUserUpdateDtos(userService.findAll()));
        model.addAttribute("userUpdate", new UserUpdateDto());

        return "users";
    }

    @PostMapping(value = "/users") //jsp not support patch or put
    public String update(@ModelAttribute UserUpdateDto userUpdateDto, Model model) {
        userService.update(userUpdateDto);

        model.addAttribute("users", userUpdateResponseMapper.userResponseDtosToUserUpdateDtos(userService.findAll()));
        model.addAttribute("userUpdate", new UserUpdateDto());

        return "users";
    }

    @PostMapping(value = "/users/delete/{id}") //jsp not support delete
    public String delete(@PathVariable long id) {
        if (userService.delete(id)) {
            return "success_delete";
        }

        return "error";
    }
}
