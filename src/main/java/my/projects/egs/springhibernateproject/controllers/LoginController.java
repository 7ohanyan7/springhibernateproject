package my.projects.egs.springhibernateproject.controllers;

import my.projects.egs.springhibernateproject.dtos.request.LoginPasswordDto;
import my.projects.egs.springhibernateproject.dtos.request.UserRequestDto;
import my.projects.egs.springhibernateproject.dtos.response.UserResponseDto;
import my.projects.egs.springhibernateproject.entities.Address;
import my.projects.egs.springhibernateproject.exceptions.LoginException;
import my.projects.egs.springhibernateproject.exceptions.RegistrationException;
import my.projects.egs.springhibernateproject.forms.Field;
import my.projects.egs.springhibernateproject.forms.register.RegisterForm;
import my.projects.egs.springhibernateproject.forms.register.RegistrationFormProvider;
import my.projects.egs.springhibernateproject.services.UserService;
import my.projects.egs.springhibernateproject.validators.register.RegistrationFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class LoginController {

    private UserService userService;
    private RegistrationFormValidator registrationFormValidator;
    private RegistrationFormProvider registrationFormProvider;

    @Autowired
    public LoginController(UserService userService, RegistrationFormValidator registrationFormValidator, RegistrationFormProvider registrationFormProvider) {
        this.userService = userService;
        this.registrationFormValidator = registrationFormValidator;
        this.registrationFormProvider = registrationFormProvider;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(@ModelAttribute RegisterForm registerForm,
                           Model model){
        List<Field> form = registrationFormProvider.getForm();
        UserRequestDto userRequestDto = registerForm.getUser();
        Address address = registerForm.getAddress();
        userRequestDto.setAddress(address);

        if (registrationFormValidator.isValid(userRequestDto)) {
            try {
                userService.register(userRequestDto);
                model.addAttribute("user", new LoginPasswordDto());
                return "login";
            } catch (RegistrationException e) {
                model.addAttribute("errorMessage", e.getMessage());
                model.addAttribute("form", form);
                model.addAttribute("regForm", new RegisterForm(new UserRequestDto(), new Address()));
                return "register";
            }
        } else {
            registrationFormProvider.addValues(form, userRequestDto);
            model.addAttribute("formErrors", registrationFormValidator.getErrors());
            model.addAttribute("form", form);
            model.addAttribute("regForm", new RegisterForm(new UserRequestDto(), new Address()));
            return "register";
        }

    }

    @RequestMapping(value = {"/register", "/"}, method = RequestMethod.GET)
    public String register(Model model){
        model.addAttribute("form", registrationFormProvider.getForm());
        model.addAttribute("regForm", new RegisterForm(new UserRequestDto(), new Address()));
        return "register";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model){

        model.addAttribute("user", new LoginPasswordDto());
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@ModelAttribute LoginPasswordDto loginPasswordDto, Model model){
        try {
            UserResponseDto userResponseDto = userService.login(loginPasswordDto);
            model.addAttribute("userInfo", userResponseDto);
            return "welcome";
        } catch (LoginException e) {
            model.addAttribute("errorMessage", e.getMessage());
            model.addAttribute("user", new LoginPasswordDto());
            e.printStackTrace();
            return "login";
        }
    }

}
