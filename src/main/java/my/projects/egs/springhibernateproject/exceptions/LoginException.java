package my.projects.egs.springhibernateproject.exceptions;

public class LoginException extends Exception{
    public LoginException(String message) {
        super(message);
    }
}
