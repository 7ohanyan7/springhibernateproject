package my.projects.egs.springhibernateproject.errors;

public interface Error {

    int getErrorCode();

    void setErrorCode(int errorCode);

    String getErrorMessage();

    void setErrorMessage(String errorMessage);

}
