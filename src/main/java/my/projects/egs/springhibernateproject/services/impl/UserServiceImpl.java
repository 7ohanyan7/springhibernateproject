package my.projects.egs.springhibernateproject.services.impl;

import my.projects.egs.springhibernateproject.dtos.request.LoginPasswordDto;
import my.projects.egs.springhibernateproject.dtos.request.UserRequestDto;
import my.projects.egs.springhibernateproject.dtos.request.UserUpdateDto;
import my.projects.egs.springhibernateproject.dtos.response.UserResponseDto;
import my.projects.egs.springhibernateproject.entities.Role;
import my.projects.egs.springhibernateproject.entities.User;
import my.projects.egs.springhibernateproject.exceptions.*;
import my.projects.egs.springhibernateproject.mappers.UserRequestMapper;
import my.projects.egs.springhibernateproject.mappers.UserResponseMapper;
import my.projects.egs.springhibernateproject.mappers.UserUpdateUserMapper;
import my.projects.egs.springhibernateproject.repositories.UserRepository;
import my.projects.egs.springhibernateproject.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private UserDetailsService userDetailsService;
    private AuthenticationManager authenticationManager;
    private UserRequestMapper userRequestMapper;
    private UserResponseMapper userResponseMapper;
    private UserUpdateUserMapper userUpdateUserMapper;
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserDetailsService userDetailsService, AuthenticationManager authenticationManager, UserRequestMapper userRequestMapper, UserResponseMapper userResponseMapper, UserUpdateUserMapper userUpdateUserMapper, BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.userDetailsService = userDetailsService;
        this.authenticationManager = authenticationManager;
        this.userRequestMapper = userRequestMapper;
        this.userResponseMapper = userResponseMapper;
        this.userUpdateUserMapper = userUpdateUserMapper;
        this.passwordEncoder = passwordEncoder;
    }
    @Override
    public Optional<UserResponseDto> save(UserRequestDto userRequestDto) {
        Optional<User> userOptional = userRepository.save(userRequestMapper.userRequestDtoToUser(userRequestDto));
        return userOptional.map(user -> userResponseMapper.userToUserResponseDto(user));
    }

    @Override
    public Optional<UserResponseDto> findById(long id) {
        Optional<User> userOptional = userRepository.findById(id);
        return userOptional.map(user -> userResponseMapper.userToUserResponseDto(user));
    }

    @Override
    public Optional<UserResponseDto> findByLogin(String login) {
        Optional<User> userOptional = userRepository.findByLogin(login);
        return userOptional.map(user -> userResponseMapper.userToUserResponseDto(user));
    }

    @Override
    public List<UserResponseDto> findAll() {
        return userResponseMapper.usersToUserResponseDtos(userRepository.findAll());
    }

    @Override
    public boolean delete(long id) {
        return userRepository.delete(id);
    }

    @Override
    public Optional<UserResponseDto> update(UserUpdateDto userUpdateDto) {
        Optional<User> userForUpdateOptional = userRepository.findById(userUpdateDto.getId());
        Optional<User> updatedUserOptional = Optional.empty();
        if (userForUpdateOptional.isPresent()) {
            User userForUpdate = userUpdateUserMapper.userUpdateDtoToUser(userUpdateDto, userForUpdateOptional.get());
            updatedUserOptional = userRepository.update(userForUpdate);
        }
        return updatedUserOptional.map(user -> userResponseMapper.userToUserResponseDto(user));
    }

    @Override
    public void register(UserRequestDto userRequestDto) throws RegistrationException {
        if (!findByLogin(userRequestDto.getLogin()).isPresent())
        {
            userRequestDto.setPassword(passwordEncoder.encode(userRequestDto.getPassword()));
            userRequestDto.setRoles(Collections.singletonList(new Role("USER")));
            save(userRequestDto);
        } else {
            throw new UserExistException("User already exist");
        }
    }

    @Override
    public UserResponseDto login(LoginPasswordDto loginPasswordDto) throws LoginException {
        UserDetails userDetails = userDetailsService.loadUserByUsername(loginPasswordDto.getLogin());
        if (userDetails == null) {
            throw new UserNotFoundException("Login not found");
        }
        if (passwordEncoder.matches(loginPasswordDto.getPassword(), userDetails.getPassword())) {
            Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails,loginPasswordDto.getPassword(), userDetails.getAuthorities());
            authentication = authenticationManager.authenticate(authentication);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return userResponseMapper.userToUserResponseDto((User) authentication.getPrincipal());
        }else throw new PasswordNotMatchException("Password not match");
    }

    @Override
    public UserResponseDto getUser() throws NotAuthenticatedException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof User)
        {
            System.out.println(((User) authentication.getPrincipal()).getRoles());
            return userResponseMapper.userToUserResponseDto((User) authentication.getPrincipal());
        }else throw new NotAuthenticatedException("User not found");
    }
}
