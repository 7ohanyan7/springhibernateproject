package my.projects.egs.springhibernateproject.forms.register;

import my.projects.egs.springhibernateproject.dtos.request.UserRequestDto;
import my.projects.egs.springhibernateproject.forms.Field;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RegistrationFormProvider {

    private List<Field> form;

    public List<Field> getForm() {
        if (form == null) {
            form = new ArrayList<>();
            for (int i = 0;i < RegistrationForm.values().length;i++) {
                form.add(new Field(RegistrationForm.values()[i].getFieldNameForView(), RegistrationForm.values()[i].getFieldName(), RegistrationForm.values()[i].getFieldType(), RegistrationForm.values()[i].getFieldClassType(), RegistrationForm.values()[i].getFieldErrorCode()));
            }
        }
        return form;
    }

    public void addValues(List<Field> form, UserRequestDto userRequestDto) {
        form.get(0).setFieldValue(userRequestDto.getLogin());
        form.get(1).setFieldValue(userRequestDto.getPassword());
        form.get(2).setFieldValue(userRequestDto.getConfirmPassword());
        form.get(3).setFieldValue(userRequestDto.getEmail());
        form.get(4).setFieldValue(userRequestDto.getFirstName());
        form.get(5).setFieldValue(userRequestDto.getLastName());
        form.get(6).setFieldValue(userRequestDto.getBirthDate());
        form.get(7).setFieldValue(userRequestDto.getAddress().getCountry());
        form.get(8).setFieldValue(userRequestDto.getAddress().getCity());
        form.get(9).setFieldValue(userRequestDto.getAddress().getRegion());
        form.get(10).setFieldValue(userRequestDto.getAddress().getStreet());
    }
}
