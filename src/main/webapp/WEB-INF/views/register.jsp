<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Register</title>
</head>
<body>
<div class="form">
    <form:form action="/register" method="post" style="float: left" modelAttribute="regForm">
        <table>
            <c:forEach var="field" items="${form}">
                <tr style="height: 30px">
                    <td>
                            ${field.getFieldNameForView()}
                    </td>
                    <c:choose>
                        <c:when test="${field.getFieldClassType() == 'User'}">
                            <td>
                                <form:input path="${'user.'.concat(field.getFieldName())}" type="${field.getFieldType()}" name="${'user.'.concat(field.getFieldName())}"
                                            value="${field.getFieldValue()}"/>
                            </td>
                            <td style="color: red;">
                                    ${formErrors.get(field.getFieldErrorCode()).getErrorMessage()}
                            </td>
                        </c:when>
                        <c:when test="${field.getFieldClassType() == 'Address'}">
                            <td>
                                <form:input path="${'address.'.concat(field.getFieldName())}" type="${field.getFieldType()}" name="${'user.'.concat(field.getFieldName())}"
                                            value="${field.getFieldValue()}"/>
                            </td>
                            <td style="color: red;">
                                    ${formErrors.get(field.getFieldErrorCode()).getErrorMessage()}
                            </td>
                        </c:when>
                    </c:choose>
                </tr>
            </c:forEach>
        </table>
        <p style="color: red">${errorMessage}</p>
        <input type="submit" value="Register">
        <a href="/login">Already registered?</a>
    </form:form>
</div>
</body>
</html>