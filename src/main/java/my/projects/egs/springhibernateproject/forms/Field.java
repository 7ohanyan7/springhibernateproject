package my.projects.egs.springhibernateproject.forms;

public class Field {
    private int fieldErrorCode;
    private String fieldNameForView;
    private String fieldName;
    private String fieldValue;
    private String fieldType;
    private String fieldClassType;

    public Field(String fieldNameForView, String fieldName, String fieldType, String fieldClassType, int fieldErrorCode) {
        this.fieldNameForView = fieldNameForView;
        this.fieldName = fieldName;
        this.fieldType = fieldType;
        this.fieldClassType = fieldClassType;
        this.fieldErrorCode = fieldErrorCode;
        this.fieldValue = "";
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getFieldNameForView() {
        return fieldNameForView;
    }

    public void setFieldNameForView(String fieldNameForView) {
        this.fieldNameForView = fieldNameForView;
    }

    public String getFieldClassType() {
        return fieldClassType;
    }

    public void setFieldClassType(String fieldClassType) {
        this.fieldClassType = fieldClassType;
    }

    public int getFieldErrorCode() {
        return fieldErrorCode;
    }

    public void setFieldErrorCode(int fieldErrorCode) {
        this.fieldErrorCode = fieldErrorCode;
    }
}
