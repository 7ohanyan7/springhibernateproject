package my.projects.egs.springhibernateproject.entities;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.List;

@org.hibernate.annotations.NamedQueries({
        @org.hibernate.annotations.NamedQuery(name = "my.projects.egs.springhibernateproject.entities.Role.findByRole",
                query = "from Role where role = :role"),
        @org.hibernate.annotations.NamedQuery(name = "my.projects.egs.springhibernateproject.entities.Role.findAllRoles",
                query = "from Role")})
@Entity
public class Role implements GrantedAuthority {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "role", unique = true, nullable = false)
    private String role;

    public Role() {}

    public Role(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getAuthority() {
        return role;
    }

}
