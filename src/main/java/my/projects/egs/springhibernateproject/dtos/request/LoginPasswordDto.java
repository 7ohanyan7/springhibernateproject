package my.projects.egs.springhibernateproject.dtos.request;


public class LoginPasswordDto {

    private String login;
    private String password;

    public LoginPasswordDto(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public LoginPasswordDto(){}

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}