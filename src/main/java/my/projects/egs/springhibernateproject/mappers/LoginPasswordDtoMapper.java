package my.projects.egs.springhibernateproject.mappers;

import my.projects.egs.springhibernateproject.dtos.request.LoginPasswordDto;
import my.projects.egs.springhibernateproject.entities.User;

import java.util.LinkedList;
import java.util.List;

public class LoginPasswordDtoMapper {

    private static LoginPasswordDtoMapper loginPasswordDtoMapper = null;

    public static LoginPasswordDtoMapper getInstance() {
        if (loginPasswordDtoMapper == null) {
            synchronized(LoginPasswordDtoMapper.class) {
                if (loginPasswordDtoMapper == null) {
                    loginPasswordDtoMapper = new LoginPasswordDtoMapper();
                }
            }
        }
        return loginPasswordDtoMapper;
    }

    private LoginPasswordDtoMapper(){}

    public LoginPasswordDto userToUsernamePasswordDto(User user){
        LoginPasswordDto loginPasswordDto = null;
        if (user != null)
        {
            loginPasswordDto = new LoginPasswordDto();
            loginPasswordDto.setLogin(user.getLogin());
            loginPasswordDto.setPassword(user.getPassword());
        }

        return loginPasswordDto;
    }

    public List<LoginPasswordDto> usersToUsernamePasswordDtos(List<User> users){
        List<LoginPasswordDto> loginPasswordDtos = new LinkedList<LoginPasswordDto>();

        for (User user :
                users) {
            loginPasswordDtos.add(userToUsernamePasswordDto(user));
        }

        return loginPasswordDtos;
    }

    public User usernamePasswordDtoToUser(LoginPasswordDto loginPasswordDto){
        User user = null;
        if (loginPasswordDto != null)
        {
            user = new User();
            user.setLogin(loginPasswordDto.getLogin());
            user.setPassword(loginPasswordDto.getPassword());
        }

        return user;
    }

    public List<User> usernamePasswordDtosToUsers(List<LoginPasswordDto> loginPasswordDtos){
        List<User> users = new LinkedList<User>();

        for (LoginPasswordDto loginPasswordDto :
                loginPasswordDtos) {
            users.add(usernamePasswordDtoToUser(loginPasswordDto));
        }

        return users;
    }

}
