package my.projects.egs.springhibernateproject.forms.register;

import my.projects.egs.springhibernateproject.dtos.request.UserRequestDto;
import my.projects.egs.springhibernateproject.entities.Address;

public class RegisterForm {
    private UserRequestDto user;
    private Address address;

    public RegisterForm(UserRequestDto user, Address address) {
        this.user = user;
        this.address = address;
    }

    public UserRequestDto getUser() {
        return user;
    }

    public void setUser(UserRequestDto user) {
        this.user = user;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
