package my.projects.egs.springhibernateproject.repositories.impl;

import my.projects.egs.springhibernateproject.entities.Address;
import my.projects.egs.springhibernateproject.repositories.AddressRepository;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class AddressRepositoryImpl implements AddressRepository {
    private static Logger logger = Logger.getLogger(AddressRepositoryImpl.class.getName());

    private SessionFactory sessionFactory;

    public AddressRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Optional<Address> findByAddress(Address address) {
        try (Session session = sessionFactory.openSession()){
            address = session.createNamedQuery("my.projects.egs.springhibernateproject.entities.Address.findByAddress", Address.class)
                    .setParameter("country", address.getCountry())
                    .setParameter("city", address.getCity())
                    .setParameter("region", address.getRegion())
                    .setParameter("street", address.getStreet())
                    .getSingleResult();
            logger.info("Got address from db");
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return Optional.ofNullable(address);
    }
}
