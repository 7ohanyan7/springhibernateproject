package my.projects.egs.springhibernateproject.errors;

import my.projects.egs.springhibernateproject.util.RegistrationConstants;

public enum ErrorCode {
    BAD_LOGIN(400_01, "Login must be large than " + RegistrationConstants.LOGIN_MIN_SIZE + " and small than " + RegistrationConstants.LOGIN_MAX_SIZE),
    BAD_PASSWORD(400_02, "Password must be large than " + RegistrationConstants.PASSWORD_MIN_SIZE),
    BAD_CONFIRM_PASSWORD(400_03, "Passwords not match"),
    BAD_FIRST_NAME(400_04, "First name must be filled"),
    BAD_LAST_NAME(400_05, "Last name must be filled"),
    BAD_BIRTH_DATE(400_06, "Birth date is not valid"),
    BAD_EMAIL(400_07, "Email must be filled"),
    BAD_COUNTRY(400_08, "Country must be filled"),
    BAD_CITY(400_09, "City must be filled"),
    BAD_REGION(400_10, "Region must be filled"),
    BAD_STREET(400_11, "Street must be filled");

    private int errorCode;
    private String errorMessage;

    ErrorCode(int errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
