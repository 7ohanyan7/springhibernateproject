package my.projects.egs.springhibernateproject.exceptions;

public class UserNotFoundException extends LoginException{
    public UserNotFoundException(String message) {
        super(message);
    }
}
